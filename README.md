# README #

This project requires the Godot game engine in order to compile / run. The entire project is contained in the _DesktopBuddy_ folder. An example template for creating a custom friend can be found in the _Template_ folder.

### ABOUT ###

Desktop Buddy is a simple application that adds a visual friend to your desktop to keep you company as you work. This friend can walk around, climb your monitor, and various other things.

Currently the system supports a fair amount of behavior modification though the use of a simple friend.cfg. You can add movement, animation frames, and some signal catching to add some interactivity. If you want something more complex the system fully supports importing PCK files w/ AI overrides so you can make your AI do anything you wish.

### DEVELOPMENT ###

This is a hobby project of mine simply because I love things like desktop pals and AI systems. The Linux platform is my target with everything else secondary.

In terms of functionality, I do want to add very complex behavior down the road. Things like reacting to audio playing, maybe even to the apps you have open or window positions (TBD), as well as more interactive things like playing games and monitoring feeds / notifications.

As to whether this will be part of the 'built-in' feature-set or simply as part of a custom friend's AI is not set in stone yet.

### ENVIRONMENT TESTING ###

* i3/picom	[success]
* xfce		[success]
* gnome		[success]
* kde/kwin	[success]
* windows	[untested]
* mac os	[untested]

If you test this on any other platforms feel free to let me know how it went!
