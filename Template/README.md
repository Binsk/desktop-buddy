# TEMPLATES #

Welcome to custom friend templates! It is possible to create your own friends for this system and, while not 100% straight-forward, I have made it as simple as I possibly can.

There are two primary parts to a custom AI / skin:

1. The config file
2. The PCK file

### CONFIG FILE ###

Unless you are creating an extremely complex or specific AI system, all of your AI behavior will be contained in the AI file. This file is your 'first line of defense' where any more complicated behavior can override the AI file through GDScript in your PCK file.

Details and specifics have not yet been written so please take a look at the example mikuo.cfg file for an example of how the main demo AI works.

Each AI can have an arbitrary number of "states". Each state is tied to an image from your PCK file and can have various elements assigned to them such as auto-switch timers, basic movement phyics, and event detection (such as mouse clicks, border interceptions, frame changes, etc).

By switching from one state to another your AI can execute various behaviors.

### PCK FILE ###

A PCK file is a .zip or .pck file exported from the Godot editor that will contain all the image files for your AI as well as a custom AI override (if any). If you want to make custom interactable elements and the like these can also be a part of this PCK file and triggered through the built-in menu system.

Your PCK file should always be structured in the following way:

res://Friends/[your friend name]/*

In the case of the Mikuo AI the it is structured as:

res://Friends/Mikuo/*

All your resources should fall inside that folder to prevent conflicts with system scripts and/or any other friends that may be imported.

If you wish to have a custom AI script you should create a scene called FriendAI.tscn with a Nod as the root and the script inheriting from "System.AI". If this scene exists it will be auto-created upon the AI spawning. If it does not exist then a default system will be used.

For all of the images that go with your state, they should be exported SpriteFrame resources. You can then specify these resources to be paired with each state. The state will auto-load the resource, measure it, align it, and handle the click-detection for it. Animation speed, looping, etc. should be defined in the exported SpriteFrames resource itself albeit it is possible for states to somewhat override these if desired.

# EXPORTING #

Once you are done with your AI, export the PCK through Godot and include it along with your *.cfg file. Any config file found in the main directory of the "Desktop Buddy" application will be auto-read, imported, and spawned (unless the "do_spawn" flag is set to 0). 
