extends Node

signal menu_clicked # (ai_instance : Node, text : String) - Thrown when the menu of a friend is clicked
class Friend extends Resource:
	var json_data : Dictionary
	var error_string : String = ""
	var is_builtin : bool = false 	# Special flag for friends included in-package
	
	func _init(data : Dictionary):
		json_data = data
		print("Scanning new friend data...")
		
		# Check for validity:
		error_string = _process_base_data()
	
	func _process_base_data() -> String:
		# Check the friend has a name:
		if not json_data.has("name"):
			return "missing friend name"

		print ("Friend name: " + json_data["name"])
		
#		if not json_data.has("states"):
#			return "missing friend states"
#		
		if json_data.has("states"):
			print ("States:")
			for state in json_data["states"]:
				if state is String:
					print("\t" + state)
				else:
					print("invalid state found")
			
		if not json_data.has("spawn_state"):
			return "missing spawn state"
	
		# Check for a valid data path:
		var file = File.new()
		var path = get_resource_path()
			
		print ("Locating: " + path)
		if not file.file_exists(path):
			return "" # Use defaults
			
		if not ProjectSettings.load_resource_pack(get_resource_path(), true):
			return "failed to load friend. Invalid data ZIP?"
		
		return "" # If the AI file doesn't exst, defaults will be used
	# Returns if the data provided contains the minimum 
	# required elements
	func get_error_string() -> String:
		return error_string
	
	# Return the name of the Friend
	func get_name() -> String:
		if error_string != "":
			return ""
			
		return json_data["name"]
	
	func get_spawn_position() -> Vector2:
		var spawn = Vector2(System.get_world_size().x * 0.5, System.get_world_size().y)
		
		if error_string != "":
			return spawn
		
		if json_data.has("x_spawn"):
			spawn.x = clamp(float(json_data["x_spawn"]), 0, 1) * System.get_world_size().x
		
		return spawn
		
	func get_do_spawn() -> bool:
		if error_string != "":
			return false
		
		if json_data.has("do_spawn"):
			return bool(json_data["do_spawn"])
		
		return true
	
	# Returns the resource path, if defined, or ""
	func get_resource_path() -> String:
		if error_string != "":
			return ""
			
		if json_data.has("pck_path"):
			return json_data["pck_path"]
		
		if json_data.has("pck_path_local"):
			return OS.get_executable_path().get_base_dir() + "/" + json_data["pck_path_local"]
			
		return OS.get_executable_path().get_base_dir() + "/" + get_name().to_lower() + ".zip"
		
	func get_menu_list() -> Array:
		if error_string != "":
			return []
			
		if json_data.has("menu_list"):
			return json_data["menu_list"].duplicate()
		
		return []
		
	# Return the raw data structure as a copy:
	func get_data() -> Dictionary:
		if error_string != "":
			return {}
			
		return json_data.duplicate()

const DEFAULT_FRIEND_NAME = "Demo"
const AI = preload("res://API/AI.gd")

var delta_time = 0;
var friend_default : Friend
var friend_map : Dictionary	# Map of all friend structures
var friend_ai_map : Dictionary # Map of AI node -> Controlled friend pairs
var friend_spawn_counter : int = 0 # Number of friends spawned

signal signal_tick
var tick_counter : float = 1	# Tick signals once every second (or so) for regular system updates
var is_mask_autoupdate : bool = false # If true, auto-updates click mask
var is_mask_active : bool = false	# If true, we are not using the full window for clicks
var is_left_click_used : bool = false	# Prevents clicking on multiple friends at once
var is_right_click_used : bool = false
var menu_count : int = 0	# Number of open menus

var mouse_velocity : Vector2 = Vector2()
onready var mouse_position_previous : Vector2 = get_viewport().get_mouse_position()

# Read the friend.cfg file and load in data as JSON:
# Read friend config data and return a structure (or the default). By default
# reads from project directiory's 'friend.cfg' but this can be overridden
func load_friend(filename : String = "friend.cfg", dir : String = OS.get_executable_path().get_base_dir() + "/"):

	var file : File = File.new()
	print("Locating: " + dir + filename)
	if file.open(dir + filename, File.READ) != OK:
		print ("No config file found! Using defaults.")
		return null
		
	var content : String = file.get_as_text()
	if validate_json(content) != "":
		printerr ("Invalid config file! Check your JSON!")
		return null
	
	var friend_json = parse_json(content)
	file.close()
	
	var friend : Friend = Friend.new(friend_json)
	if friend.get_error_string() != "":
		printerr (friend.get_error_string())
		return null
	
	return friend

# Spawns a new copy of a friend
# If specified (in percentage) the x_pos will override the specified position
func spawn_friend(name : String = DEFAULT_FRIEND_NAME, x_pos = null):
		# Fried doesn't exist:
	if not friend_map.has(name):
		return
	print("Spawning: " + name)
	var friend : Friend = friend_map[name]
	var file : File = File.new()
	var ai_node : Resource
	print("AI Path: " + "res://Friends/" + friend.get_name() + "/FriendAI.tscn")
	if file.file_exists("res://Friends/" + friend.get_name() + "/FriendAI.tscn"):
		ai_node = load("res://Friends/" + friend.get_name() + "/FriendAI.tscn")
	else:
		ai_node = load("res://Scenes/FriendAI.tscn")
		if ai_node == null:
			printerr("Failed to load friend! Missing default AI scene!")
			return
			
		print ("No custom AI found, using default.")

	var friend_instance = load("res://Scenes/Friend.tscn").instance()
	friend_instance.resource_path = "res://Friends/" + friend.get_name() + "/"
	var ai_instance = ai_node.instance()

	friend_ai_map[ai_instance] = {"instance" : friend_instance, "template" : friend}
	ai_instance.visual_node = friend_instance
	# Generate the states from JSON:
	if friend.json_data["spawn_state"] is String:
		ai_instance.state_spawn = [friend.json_data["spawn_state"]]
	else:
		ai_instance.state_spawn = friend.json_data["spawn_state"]
	
	if friend.json_data.has("states"):
		var state_dict = friend.json_data["states"];
		for state_name in state_dict.keys():
			var state_data = state_dict[state_name]
			var state = AI.State.new(ai_instance, state_name, state_data)
			var path = ""
			var flip_h = false
			var align = Vector2(0.5, 1)
			if not state_data.has("path"):
				printerr ("State " + state_name + " missing image path...")
				continue
			path = state_data["path"]
			if state_data.has("mirror"):
				flip_h = bool(state_data["mirror"])
			if state_data.has("align") and state_data["align"] is Array and state_data["align"].size() == 2:
				align = Vector2(float(state_data["align"][0]), float(state_data["align"][1]))
			ai_instance.register_state(state, path, flip_h, align)
	
	$"/root/Game/Interactive".add_child(friend_instance)
	$"/root/Game/Control".add_child(ai_instance)
	
	friend_instance.position = friend.get_spawn_position()
	if x_pos != null:
		x_pos = clamp(float(x_pos), 0, 1)
		friend_instance.position.x = get_world_size().x * x_pos
	
	friend_spawn_counter += 1

# Spawns a menu with the number of items listed in the array:
# Returns the instance. The instance has a signal that will be thrown when
# an item is clicked.
func spawn_menu(items : Array, owner : Node):
	var scene = load("res://Scenes/Menu.tscn")
	var instance = scene.instance()
	instance.rect_position = get_viewport().get_mouse_position()
	for item in items:
		instance.add_button(item)
	
	$"/root/Game/Interactive".add_child(instance)
	if instance.connect("signal_button_pressed", self, "_signal_menu_button_pressed", [owner]) != OK:
		printerr("Failed to connect signal!")
		
	set_mouse_passthrough_world()
	menu_count += 1
	
	
func _signal_menu_button_pressed(text : String, owner):
	is_left_click_used = true
	# Let AI instances now a button was pressed:
	for key in friend_ai_map.keys():
		var data = friend_ai_map[key]
		if data.instance != owner:
			continue
			
		if text == "Kill " + data.template.get_name():
			key.queue_free()
			data.instance.queue_free()
			friend_ai_map.erase(key)
			friend_spawn_counter -= 1
			break
		else:
			emit_signal("menu_clicked", key, text)
	
	if friend_spawn_counter <= 0:
		get_tree().quit()
	
	if menu_count <= 0:
		set_mouse_passthrough_masked()
	
func get_interface_node() -> Node:
	return $"/root/Game/Interface"
	
# Return the visual friend node given the id of the FriendAI scene.
# E.g., in your FriendAI scene you would call get_friend_node(self)
func get_friend_node(ai_node : AI):
	if friend_ai_map.has(ai_node):
		return friend_ai_map[ai_node].instance
		
	return null
	
# Returns the template data so you can grab things like friend name, etc.
func get_friend_template(ai_node : AI):
	if friend_ai_map.has(ai_node):
		return friend_ai_map[ai_node].template
	
	return null

func get_friend_exists_by_name(name : String) -> bool:
	for data in friend_ai_map.values():
		var template = data.template
		if template.get_name() == name:
			return true
	return false
func get_delta_time() -> float:
	return delta_time

# Return the size of the render world:
func get_world_size() -> Vector2:
	return OS.get_window_size()
	
func get_world_size_desired() -> Vector2:
	return OS.get_screen_size()

func get_mouse_position() -> Vector2:
	return get_viewport().get_mouse_position()

func get_mouse_velocity() -> Vector2:
	return mouse_velocity

# Sets the mouse passthrough to the entire window; used for drag + drop:
func set_mouse_passthrough_world():
	is_mask_active = false
	OS.set_window_mouse_passthrough(PoolVector2Array())

# Sets the click area to a dynamic shape based on "Interactive" instances. 
# This has some caviats! ALL nodes in Interactive shapes should follow 4 rules:
#	1. 	They MUST have a function called "get_clickable_shape()" that returns a
#		PoolVector2Array, even if empty.
#	2.	The shape must be defined counter-clockwise starting at the bottom
#	3.	The shape must include a 'shaft' of 1 pixel that reaches down to
#		the bottom of the world. This is used to connect all shapes together.
#	4.	The vertical shaft x-position MUST be at the node's x position, exactly,
#		as this is how instances are sorted.
func set_mouse_passthrough_masked():
	is_mask_active = true
	
	var child_array : Array = $"/root/Game/Interactive".get_children()
	var process_array : Array = []
	for child in child_array:
		process_array.push_back(child.get_clickable_rect())
		
	# Very expensive rectangle combination. With only a handful of friends this
	# is fine; if we start getting a LOT then this will be laggy:
	while true:
		var is_merged : bool = false
		var merged_array : Array = []
		var index_ignore : Array = []
		for i in range(0, process_array.size()):
			if index_ignore.find(i) >= 0:
				continue
				
			var is_sub_merged : bool = false
			for j in range(i + 1, process_array.size()):
				if process_array[i].intersects(process_array[j], true):
					merged_array.push_back(process_array[i].merge(process_array[j]))
					index_ignore.push_back(j)

					is_merged = true
					is_sub_merged = true
					break

			if not is_sub_merged:
				merged_array.push_back(process_array[i])

		process_array = merged_array

		if not is_merged:
			break
	
	process_array.sort_custom(self, "_sort_instance_order")
	var parray : PoolVector2Array = PoolVector2Array()

	for rect in process_array:
		parray.push_back(Vector2(rect.position.x + rect.size.x, get_world_size().y))
		parray.push_back(Vector2(rect.position.x + rect.size.x, rect.position.y))
		parray.push_back(Vector2(rect.position.x, rect.position.y))
		parray.push_back(Vector2(rect.position.x, rect.position.y + rect.size.y))
		parray.push_back(Vector2(rect.position.x + rect.size.x, rect.position.y + rect.size.y))
		parray.push_back(Vector2(rect.position.x + rect.size.x, get_world_size().y))
	
	OS.set_window_mouse_passthrough(parray)

func set_mask_autoupdate(update : bool = true):
	is_mask_autoupdate = update

# Picks a random element from a list
func choose_random(options : Array):
	return options[randi() % options.size()]
	
func choose_random_nested(options : Array):
	var result = options
	while result is Array:
		result = choose_random(result)
	
	return result

# Right-to-left order sorter for click-shape render order
func _sort_rect_order(inst1 : Rect2, inst2 : Rect2):
	if inst1.position.x + inst1.size.x >= inst2.position.x + inst2.size.x:
		return true
	return false

func _ready():
	# Add default friend to map:
	var friend_data : Dictionary = {
		"name" : DEFAULT_FRIEND_NAME,
		"spawn_state" : ["idle_left", "idle_right"]
	}
	friend_default = Friend.new(friend_data)
	friend_default.is_builtin = true
		# Add the default friend to the system:
	friend_map[DEFAULT_FRIEND_NAME] = friend_default
	
	# Connect mouse update signal:
	if connect("signal_tick", self, "_update_passthrough") != OK:
		print("Failed to connect signal...")

func _process(_delta):
	delta_time = _delta
	tick_counter -= _delta
	
	if tick_counter <= 0:
		tick_counter += 1
		emit_signal("signal_tick")
	
	# Menu and othe GUI items use the built-in click blocking via Controls.
	# If they are open, we can't click on friends:
	if menu_count == 0:
		is_left_click_used = false
	else:
		is_left_click_used = true
		
	is_right_click_used = false
	
	var vpos = get_viewport().get_mouse_position()
	mouse_velocity = (vpos - mouse_position_previous) / System.get_delta_time()
	mouse_position_previous = vpos
		
func _update_passthrough():
	if is_mask_autoupdate and is_mask_active:
		set_mouse_passthrough_masked()
