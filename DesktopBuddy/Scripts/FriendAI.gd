extends System.AI
# ABOUT
# This is the 'default' friend AI node that will be loaded whenever it is not
# overridden. It has some assumed images and states so that adding a custom
# skinned friend is easy while still allowing overriding for more complex friends.
#
# A lot of the definitions in this default would normally be done in a JSON file but
# they are done directly here to prevent the need for an extra file or excessive
# JSON in the System script.

func _ready():
	register_state(State.new(self, "idle_left", {
		"time" : [5, 20],
		"velocity" : [],
		"end_states" : ["idle_left", "walk_left", "walk_right"],
		"events" : {
			"left_clicked" : {
				"state" : "dragging_left",
			}
		}
	}), "idle_left.res")
	register_state(State.new(self, "idle_right", {
		"time" : [5, 20],
		"velocity" : [],
		"end_states" : ["idle_left", "walk_left", "walk_right"],
		"events" :{
			"left_clicked" : {
					"state" : "dragging_right",
				}
			}
	}), "idle_right.res")
	register_state(State.new(self, "walk_left", {
		"time" : [5, 20],
		"velocity" : [-30, 0],
		"end_states" : ["idle_left", "walk_left", "walk_right"],
		"events" : {
			"left_clicked" : {
					"state" : "dragging_left",
				},
			"boundary_west": {
					"state" : ["idle_left", "walk_right"]
				}
			}
	}), "walk_left.res")
	register_state(State.new(self, "walk_right", {
		"time" : [5, 20],
		"velocity" : [30, 0],
		"end_states" : ["idle_right", "walk_left", "walk_right"],
		"events" : {
			"left_clicked" : {
					"state" : "dragging_right",
				},
			"boundary_east": {
					"state" : ["idle_right", "walk_left"]
				}
			}
	}), "walk_right.res")
	register_state(State.new(self, "dragging_left", {
		"velocity" : [],
		"position" : ["mouse.clickpos.x", "mouse.clickpos.y"],
		"events" : {
			"left_released" : {
				"state" : "falling_left",
				"velocity" : ["mouse.velocity.x", "mouse.velocity.y"]
			}
		}
	}), "idle_left.res")
	register_state(State.new(self, "dragging_right", {
		"velocity" : [],
		"position" : ["mouse.clickpos.x", "mouse.clickpos.y"],
		"events" : {
			"left_released" : {
				"state" : "falling_right",
				"velocity" : ["mouse.velocity.x", "mouse.velocity.y"]
			}
		}
	}), "idle_right.res")
	register_state(State.new(self, "falling_left", {
		"acceleration" : [0, 25],
		"friction" : [4, 0],
		"events" : {
			"boundary_south" : {
				"state" : ["idle_left", "idle_right", "walk_left", "walk_right"]
			},
			"boundary_north" : {
				"velocity" : ["friend.velocity.x", 0]
			}
		}
	}), "idle_left.res")
	register_state(State.new(self, "falling_right", {
		"acceleration" : [0, 25],
		"friction" : [4, 0],
		"events" : {
			"boundary_south" : {
				"state" : ["idle_left", "idle_right", "walk_left", "walk_right"]
			},
			"boundary_north" : {
				"velocity" : ["friend.velocity.x", 0]
			}
		}
	}), "idle_right.res")

	set_state("idle_left")
