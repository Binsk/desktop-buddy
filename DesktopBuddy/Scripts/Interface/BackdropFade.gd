extends ColorRect


var fade_target : float = 0.0

func _process(_delta):
	if self_modulate.a == fade_target:
		set_process(false)
		
	if self_modulate.a > fade_target:
		self_modulate.a = max(self_modulate.a - _delta, fade_target)
	else:
		self_modulate.a = min(self_modulate.a + _delta, fade_target)

func set_fade_target(value : float):
	fade_target = value
	set_process(true)
