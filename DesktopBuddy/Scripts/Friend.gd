extends Node2D
# === PROPERTIES === #
var prop_velocity : Vector2 = Vector2() # Movement velocity in pixels / second

# === MEMBER VARIABLES === #
var state_sprite_map : Dictionary = Dictionary()
var state_current : int = 0
var sprite_size : Vector2 = Vector2()
var resource_path : String

signal signal_left_clicked	# Thrown when there is a mouse click over the character
signal signal_left_released	# Thrown when the mouse button was released after a click
signal signal_hit_world_boundary # Thrown when friend crosses world boundary; passes Vector2() for boundary direction
signal signal_frame_changed # Thrown when the frame changes

var is_mouse_left_pressed : bool = false	# Used to determine clicked vs hold

# === PUBLIC METHODS === #

# Register an animation frame to a state so it will be displayed whenever that
# state is active.
func register_sprite_state(state : int, frames_path : String, flip_h : bool = false, alignment : Vector2 = Vector2(0.5, 1)):
	# First check for a valid sprite frames
	var frames : SpriteFrames = load(resource_path + frames_path)
	if frames == null: # If invalid, don't do anything further
		return false
			
	# Check some basic properties. Note, all sprite frames across sprite sheets
	# should be contained in an equal-sized frame:
	var ani_names = frames.get_animation_names()
	if ani_names.size() <= 0: # No animation data, skip
		return false
		
	if frames.get_frame_count(ani_names[0]) <= 0: # Invalid frame count, skip:
		return false
		
		# If resetting the sprite, delete the old one:
	if state_sprite_map.has(state):
		state_sprite_map[state].queue_free()
		
		# Simply grab the first frame; assuming they are all the same size we
		# can base our measurements off this:
	var texture : Texture = frames.get_frame(ani_names[0], 0)
	sprite_size = texture.get_size()

	# All is good, create our visual instance and register it:
	var instance : AnimatedSprite = AnimatedSprite.new()
	instance.set_sprite_frames(frames)
	instance.playing = true
	instance.centered = true
	instance.visible = false
	instance.flip_h = flip_h
	instance.offset.x = lerp(-sprite_size.x * 0.5, sprite_size.x * 0.5, alignment.x)
	instance.offset.y = lerp(sprite_size.y * 0.5, -sprite_size.y * 0.5, alignment.y)
	instance.connect("frame_changed", self, "_signal_frame_changed")
	
	add_child(instance)
	instance.position = Vector2()
	state_sprite_map[state] = instance
	return true

func get_state() -> int:
	return state_current

func set_state(state : int):
	# Hide all sprites:
	for instance in state_sprite_map.values():
		instance.visible = false
	
	# If we have a sprite assigned for this state, make it visible:
	if state_sprite_map.has(state):
		state_sprite_map[state].visible = true
	
	state_current = state
	set_anim_frame(0) # Always reset the frame`

# Sets the 'flipped' state for the current state:
func set_flip_h(flip_h : bool = false):
	if not state_sprite_map.has(state_current):
		return
	
	state_sprite_map[state_current].flip_h = flip_h

# Set if the animation is playing or not
func set_is_anim_playing(playing : bool = true):
	if not state_sprite_map.has(state_current):
		return
	
	if playing:
		state_sprite_map[state_current].play()
	else:
		state_sprite_map[state_current].stop()

# Sets the animation frame of the state (values wrap around)
func set_anim_frame(frame : int = 0):
	if not state_sprite_map.has(state_current):
		return
	
	state_sprite_map[state_current].set_frame(frame)

func get_anim_frame() -> int:
	if not state_sprite_map.has(state_current):
		return -1
		
	return state_sprite_map[state_current].get_frame()

# === PRIVATE METHODS === #
func _process(delta):
	
	# Process movement:
	position += prop_velocity * delta
	
	# Detect boundary crossing:
	var world_size : Vector2 = System.get_world_size()
	
	if position.x < sprite_size.x * 0.5:
		position.x = sprite_size.x * 0.5
		emit_signal("signal_hit_world_boundary", Vector2(-1, 0))
	
	if position.x > world_size.x - sprite_size.x * 0.5:
		position.x = world_size.x - sprite_size.x * 0.5
		emit_signal("signal_hit_world_boundary", Vector2(1, 0))
		
	if position.y < sprite_size.y:
		position.y = sprite_size.y
		emit_signal("signal_hit_world_boundary", Vector2(0, -1))
	
	if position.y > world_size.y:
		position.y = world_size.y 
		emit_signal("signal_hit_world_boundary", Vector2(0, 1))
	
	# Detect mouse clicks:
	var mouse_position : Vector2 = get_viewport().get_mouse_position()
	if (mouse_position.x >= position.x - sprite_size.x * 0.5 and
		mouse_position.x <= position.x + sprite_size.x * 0.5 and
		mouse_position.y >= position.y - sprite_size.y and
		mouse_position.y <= position.y):
		if not System.is_left_click_used and Input.is_action_just_pressed("mb_left") and not is_mouse_left_pressed:
			is_mouse_left_pressed = true
			emit_signal("signal_left_clicked")
			System.is_left_click_used = true
		if not System.is_right_click_used and Input.is_action_just_pressed("mb_right"):
			
			var fdata : Dictionary
			for data in System.friend_ai_map.values():
				if data.instance == self:
					fdata = data
					break
					
			var array = fdata.template.get_menu_list()
			array.push_back("Kill " + fdata.template.get_name())
			System.spawn_menu(array, self)
			System.is_right_click_used = true
			
	if is_mouse_left_pressed and not Input.is_mouse_button_pressed(BUTTON_LEFT):
		is_mouse_left_pressed = false
		emit_signal("signal_left_released")

func get_clickable_rect() -> Rect2:
	return Rect2(position.x - sprite_size.x * 0.5, position.y - sprite_size.y, sprite_size.x, sprite_size.y)

func _signal_frame_changed():
	emit_signal("signal_frame_changed")
