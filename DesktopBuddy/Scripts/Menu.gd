extends Control

signal signal_button_pressed # (text : String)
var button_count : int = 0

func _ready():
	$Popup.connect("id_pressed", self, "_button_pressed")
	$Popup.rect_position = System.get_mouse_position()
	$Popup.popup()

func get_clickable_rect() -> Rect2:
	return Rect2($Popup.rect_position, $Popup.rect_size)

func add_button(text : String):
	button_count += 1	
	rect_size.y = button_count * 26
	$Popup.add_item(text)

#func _button_pressed(text : String):
func _button_pressed(index : int):
	System.menu_count -= 1
	queue_free()
	emit_signal("signal_button_pressed", $Popup.get_item_text(index))

func _process(_delta):
	if Input.is_action_just_pressed("mb_left") or Input.is_action_just_pressed("mb_right"):
		var mouse_position = get_viewport().get_mouse_position()
		if not Rect2($Popup.rect_position, $Popup.rect_size).has_point(mouse_position):
			System.menu_count -= 1
			queue_free()
			# Signal nothing was selected:
			emit_signal("signal_button_pressed", "")
