extends Node

# Basic structure:
"""
	# Game
	#   | - Interactive
	#   		| - Friend
	#			| - ...
	#	| - Control
	#			| - FriendAI
	#			| - ...
	#
	# Under the 'Interactive' node should only be instances that are interactive
	# with the user's mouse. The system will scan ALL child nodes for the 
	# function get_clickable_shape() to construct all clickable elements in
	# the game.
	#
	# Under the 'Control' node should be instances responsible for things such
	# as managing the Friend's movement, mini-game logic, etc. They will activate
	# or deactivated instances in Interactive as required through the use
	# of System.<> functions.
"""

# Potential Sprite Animations
"""
	# == ESSENTIAL (must have)
	# - Idle
	# -	Walking
	# - Grabbed
	# - Falling
	# - Climbing (side of monitor)
	# - Swinging (across top of the screen like monkey bars)

	# == SECONDARY (would REALLY like to have)
	# - Facing away, looking up (as if looking at something on the monitor)
	# - Sitting (eyes open / eyes closed) Can be combined w/ stun, zzz, etc
	# - Getting up (from sitting to idle)
	# - Sitting down (from idle to sitting)
	# - Opening 'thing' (might be a book, computer, or something else; not sure yet)
	# - Using 'thing' (such as 'reading', 'typing', etc)
	# - Transition from walk to climb (and back)
	# - Transition from climb to hang (and back)

	# == TERTIARY (would be fun to have)
		# The following three are mostly for engagement w/ elements in your desktop
		# E.g., a game of tic-tac-toe and the buddy could jump and 'hit' the spot
		# on their turn.
	# - Jumping off edge
	# - Jumping up, facing away from you
	# - Somehow hitting the monitor (w/ hand, mallet, or something obvious)

	# - Transition from 'sitting' to 'lying down'
	# - Lying down (mostly for some variety to sitting)

	# == SEPARATE SPRITE ANIMATIONS
	# - Zzz
	# - "?" confused bubble
	# - Stunned stars around head
	# - Thought bubble YES
	# - Thought bubble NO
"""
func _ready():
	randomize()
	Engine.set_target_fps(60)
	
	var directory : Directory = Directory.new()
	var directory_string = OS.get_executable_path().get_base_dir() + "/"
	if directory.open(directory_string) == OK:
		var fname = ""
		if directory.list_dir_begin(true, true) == OK:
			fname = directory.get_next()
			
		while fname != "":
			var fname_current = fname
			fname = directory.get_next()

			# If a directory, skip; we only read files:
			if directory.dir_exists(fname_current):
				continue
				
			# Make sure it ends in CFG (only really done to prevent reading
			# from binaries)
			if not fname_current.ends_with("cfg"):
				continue

			var file : File = File.new()
			if file.open(directory_string + fname_current, File.READ) != OK:
				printerr("Failed to open file: " + fname_current)
				continue

			var text = file.get_as_text()
			file.close()

			var result = JSON.parse(text)
			if result.error != OK: # Invalid config
				continue
			var json = result.result
			if not json.has("format"): # Not config JSON
				continue
				
			if json["format"] != "dt_buddy": # Not config JSON
				continue

			# Add friend:
			var friend : System.Friend = System.load_friend(fname_current)
			if friend == null: # There was an error loading / parsing
				continue
				
			if System.friend_map.has(friend.get_name()):
				printerr("Friend w/ name " + friend.get_name() + " already exists!")
				continue

			System.friend_map[friend.get_name()] = friend

		directory.list_dir_end()
	else:
		printerr("Failed to open local directory!")
	
	var spawn_count : int = 0
	for friend in System.friend_map.values():
			# Skip invalid friend files:
		if friend.get_error_string() != "":
			continue
			
		if friend.is_builtin:
			continue

		if not friend.json_data.has("do_spawn") or (
			friend.json_data.has("do_spawn") and float(friend.json_data["do_spawn"]) >= 0.5):
			System.spawn_friend(friend.get_name())

		spawn_count += 1
		
	# If no one spawned, spawn a default:
	if spawn_count == 0:
		System.spawn_friend()
		
	# Make system transparent:
	get_tree().get_root().set_transparent_background(true) 
	
	# Make window stretch over entire monitor w/o requiring fullscreen
	OS.set_min_window_size(System.get_world_size_desired())
	OS.set_max_window_size(System.get_world_size_desired())
	OS.set_window_size(System.get_world_size_desired())
	OS.set_window_position(Vector2(0, 0))
	OS.set_window_always_on_top((true))
	
	# Connect window resize:
	if get_tree().root.connect("size_changed", self, "_window_resized") != OK:
		printerr("Failed to connect signal...")
	
	# Make friend clickable
	System.set_mouse_passthrough_world() # First make full display clickable in case of errors
	call_deferred("form_clickable") # Allow things to initialize, then set to 'friend'

func form_clickable():
	for dict in System.friend_ai_map.values():
		var instance = dict.instance
		var template = dict.template
		instance.position = template.get_spawn_position()
#		instance.position.x += -100 + randi() % 200
		
	System.set_mouse_passthrough_masked()
	System.set_mask_autoupdate()

func _window_resized():
	form_clickable()
