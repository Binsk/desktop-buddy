extends Node
# Friend state contains some basic details about behavior for a given
# state. Note that some details, such as animation, are registered apart
# from the state struct, however.
class State extends Reference:
	# System
	var title : String = "" # The unique name of the state
	var index : int = -1
	var data_backup : Dictionary = {}
	var parent_node : Node = null
	
	# Properties
	var timer_range : Vector2 = Vector2()
	var timer_states : Array = []	# Possible 'end timer' random state changes
	var start_velocity = null	# If null, it is not used
	var position = null	# If set, forces this position (NOTE, DYNAMIC VALUE)
	var acceleration : Vector2 = Vector2()
	var frame : int = -1	# If >=0 forces the specified frame to appear
	var friction : Vector2 = Vector2()
	var event_map : Dictionary = {}	# Built-in event reactions (such as mouse pressed)
	
	# warning-ignore:shadowed_variable
	func _init(parent, title : String, dictionary : Dictionary = {}):
		parent_node = parent
		self.title = title
		from_dictionary(dictionary)

	# Takes a value and replaces it if it is a string keyword:
	func _keyword_replace(keyword) -> float:
		# Already a number, we're good
		if keyword is int or keyword is float:
			return float(keyword)
			
		# Invalid value, return default of 0:
		if not keyword is String:
			return 0.0
			
		# NOTE: This is very brute-force. Switch to an intelligent solution once
		# 		more options become available
		if keyword == "mouse.velocity.x":
			return System.get_mouse_velocity().x
		elif keyword == "mouse.velocity.y":
			return System.get_mouse_velocity().y
		elif keyword == "mouse.position.x":
			return System.get_mouse_position().x
		elif keyword == "mouse.position.y":
			return System.get_mouse_position().y
		elif keyword == "mouse.clickpos.x":
			return System.get_mouse_position().x - parent_node.mouse_clicked_offset.x
		elif keyword == "mouse.clickpos.y":
			return System.get_mouse_position().y - parent_node.mouse_clicked_offset.y
		elif keyword == "friend.position.x":
			return parent_node.visual_node.position.x
		elif keyword == "friend.position.y":
			return parent_node.visual_node.position.y
		elif keyword == "friend.velocity.x":
			return parent_node.visual_node.prop_velocity.x
		elif keyword == "friend.velocity.y":
			return parent_node.visual_node.prop_velocity.y
		elif keyword == "friend.frame.current":
			return parent_node.visual_node.get_anim_frame()
		elif keyword == "boundary.north" or keyword == "boundary.west":
			return 0.0
		elif keyword == "boundary.east":
			return System.get_world_size().x
		elif keyword == "boundary.south":
			return System.get_world_size().y
		
		# Invalid setting, return default:
		return 0.0
	# Sets variables from a dictionary. Assumes a specific syntax:
	func from_dictionary(dictionary : Dictionary):
		# If a time range is set, read it in:
		if dictionary.has("time") and dictionary["time"] is Array and dictionary["time"].size() == 2:
			var time = dictionary["time"]
			timer_range.x = _keyword_replace(time[0])
			timer_range.y = _keyword_replace(time[1])
			data_backup["time"] = [timer_range.x, timer_range.y]
		
		if dictionary.has("velocity") and dictionary["velocity"] is Array:
			if dictionary["velocity"].size() != 2:
				dictionary["velocity"] = [0, 0]
				
			var velocity = dictionary["velocity"]
			start_velocity = Vector2(_keyword_replace(velocity[0]), _keyword_replace(velocity[1]))
			data_backup["velocity"] = [start_velocity.x, start_velocity.y]
		
		if dictionary.has("acceleration") and dictionary["acceleration"] is Array and dictionary["acceleration"].size() == 2:
			var accel = dictionary["acceleration"]
			acceleration.x = _keyword_replace(accel[0])
			acceleration.y = _keyword_replace(accel[1])
			data_backup["acceleration"] = [acceleration.x, acceleration.y]
		
		if dictionary.has("friction") and dictionary["friction"] is Array and dictionary["friction"].size() == 2:
			var frict = dictionary["friction"]
			friction.x = _keyword_replace(frict[0])
			friction.y = _keyword_replace(frict[1])
			data_backup["friction"] = [friction.x, friction.y]
		
		if dictionary.has("position") and dictionary["position"] is Array and dictionary["position"].size() == 2:
			position = [dictionary["position"][0], dictionary["position"][1]]
			data_backup["position"] = position
			
		if dictionary.has("frame") and dictionary["frame"] is float:
			frame = dictionary["frame"]
			data_backup["frame"] = frame
		
		if dictionary.has("end_states") and dictionary["end_states"] is Array:
			timer_states = dictionary["end_states"]
			data_backup["end_states"] = timer_states
			
		if dictionary.has("events") and dictionary["events"] is Dictionary:
			event_map = dictionary["events"]
			data_backup["events"] = event_map
		
	# Picks a random state from the 'timer_states' array.
	# If there is no valid result, "" is returned
	func pick_random_timer_state() -> String:
		if timer_states.size() == 0:
			return ""
			
		var value = timer_states
		while value is Array:
			value = System.choose_random(value)
		
		return String(value)
	
	func duplicate() -> State:
		var state = State.new(parent_node, title, data_backup)
		state.index = index
		return state

var state_map : Dictionary = {}	# Contains all valid states for the AI
onready var visual_node = System.get_friend_node(self) # Visual representation of our friend

var state_spawn : Array = []
var state_timer : float = 0	# Seconds until random state switch
var is_state_timer_active : bool = false # Whether or not to use random state switches
var state_current : State = null	# Currently active state

var is_mouse_left_clicked : bool = false	# Use clicked and is still clicking on visual instance
var mouse_clicked_offset : Vector2 = Vector2() # Offset from character position we clicked at


# Registers a new state. The state must have a unique name or it will replace
# whatever other state was defined with the same name. The image path is a local 
# path for the image name representing the state.
# You can also specify the horizontal flip and alignment of the image.
func register_state(state : State, img_path : String, flip_h : bool = false, img_align = Vector2(0.5, 1.0)) -> bool:
	# Attempt to load / add the image to the visual instance:
	var state_index = state.index
	if state.index < 0: # If not yet assigned, generate an ID
		state_index = state_map.size()
		
	# Add the sprite to the visual instance or replace it if already set:
	if not visual_node.register_sprite_state(state_index, img_path, flip_h, img_align):
		return false
	
	# Assign an ID to the state:
	state.index = state_index
	state_map[state.title] = state
	
	return true

# Applies the effects of the specified event dictated by the state properties. 
# The system has a few built-in events but custom ones can be specified as well.
func apply_event(name : String):
	if state_current == null:
		return
		# No settings for this specific event:
	if not state_current.event_map.has(name):
		return
		
	var event = state_current.event_map[name]
	if event.has("state"):
		if event["state"] is String:
			set_state(event["state"])
		elif event["state"] is Array:
			set_state(System.choose_random_nested(event["state"]))
	
	var state = state_current.duplicate()
	state.from_dictionary(event)
	set_state_direct(state) # Temporary state override

# Returns the id of the specified state or -1 if it is not defined.
func get_state_id(name : String) -> int:
	if not state_map.has(name):
		return -1
	
	return state_map[name].index

# Sets the state as active and returns if the state was changed successfully.
func set_state(name : String) -> bool:
	var index = get_state_id(name)
	if index < 0:
		return false
	
	visual_node.set_state(index)
	set_state_direct(state_map[name])
	return true
	
func set_state_direct(state : State):
	
	# Set timer. If the range is 'empty' we assume the state is waiting on
	# an event rather than a time based trigger.
	if state.timer_range.is_equal_approx(Vector2()):
		is_state_timer_active = false
	else:
		is_state_timer_active = true
		state_timer = rand_range(min(state.timer_range.x, state.timer_range.y), max(state.timer_range.x, state.timer_range.y))
	
	if state.start_velocity != null:
		visual_node.prop_velocity = state.start_velocity
	
	apply_event("state_ended") # Apply any special changes after the state changes
	state_current = state
	if state_current.frame >= 0:
		visual_node.set_anim_frame(state_current.frame)

# Returns the currently active and processing state
func get_active_state() -> State:
	return state_current

func get_mouse_clicked_offset() -> Vector2:
	return mouse_clicked_offset

# Connect special signals for mouse interaction and border crossing:
func _ready():
	visual_node = System.get_friend_node(self)
	# Connect signals to friend:
	if visual_node.connect("signal_left_clicked", self, "_signal_left_clicked") != OK:
		printerr("Failed to connect signal...")
	if visual_node.connect("signal_left_released", self, "_signal_left_released") != OK:
		printerr("Failed to connect signal...")
	if visual_node.connect("signal_hit_world_boundary", self, "_signal_world_boundary") != OK:
		printerr("Failed to connect signal...")
	if visual_node.connect("signal_frame_changed", self, "_signal_frame_changed") != OK:
		printerr("Failed to connect signal...")
	
	set_state(System.choose_random_nested(state_spawn))
		
# Handle default state behavior. This can be inherited, overwritten, or expanded by a
# custom AI node, however it creates a lot of basic interaction.
func _process(_delta):
	if state_current == null:
		return
		
	if is_state_timer_active:
		state_timer -= _delta
		if state_timer <= 0:
			var new_state = state_current.pick_random_timer_state()
			if new_state != "": # If a valid state, change states
				set_state(new_state)
			else:
				printerr("Invalid state switch! Resetting state...")
				set_state(state_current.title)
			
	# Process base state elements:
	visual_node.prop_velocity += state_current.acceleration
	
	if abs(visual_node.prop_velocity.x) <= state_current.friction.x:
		visual_node.prop_velocity.x = 0
	else:
		visual_node.prop_velocity.x -= sign(visual_node.prop_velocity.x) * state_current.friction.x
	
	if abs(visual_node.prop_velocity.y) <= state_current.friction.y:
		visual_node.prop_velocity.y = 0
	else:
		visual_node.prop_velocity.y -= sign(visual_node.prop_velocity.y) * state_current.friction.y
	
	if state_current.position != null:
		visual_node.position = Vector2(state_current._keyword_replace(state_current.position[0]), state_current._keyword_replace(state_current.position[1]))
	
#	if state_current.frame >= 0:
#		visual_node.set_anim_frame(state_current.frame)
	
func _signal_left_clicked():
	is_mouse_left_clicked = true
	
	System.set_mouse_passthrough_world()
	System.set_mask_autoupdate(false)
	mouse_clicked_offset = System.get_mouse_position() - visual_node.position
	apply_event("left_clicked")

func _signal_left_released():
	is_mouse_left_clicked = false
	
	System.set_mouse_passthrough_masked()
	System.set_mask_autoupdate(true)
	mouse_clicked_offset = Vector2()
	apply_event("left_released")

func _signal_world_boundary(direction : Vector2):
	if direction.x < 0:
		apply_event("boundary_west")
	elif direction.x > 0:
		apply_event("boundary_east")
	if direction.y < 0:
		apply_event("boundary_north")
	elif direction.y > 0:
		apply_event("boundary_south")

func _signal_frame_changed():
		# Generic 'frame changed' signal:
	apply_event("frame_changed")
		# Specialized event if needed
	apply_event("frame_set_" + String(visual_node.get_anim_frame()))
